package main

import (
	"fmt"
	"strings"
	"unicode"
)

func twostrings() {
	var a, b string
	fmt.Scan(&a, &b)
	index := strings.Index(a, b)
	fmt.Println(index)
}
func strin() {
	var a, b string
	fmt.Scan(&a)
	for i, val := range a {
		if i%2 != 0 {
			b = b + string(val)
		}
	}
	fmt.Println(b)
}
func showones() {
	var a, b string
	fmt.Scan(&a)
	for _, val := range a {
		if strings.Count(a, string(val)) == 1 {
			b = b + string(val)
		}
	}
	fmt.Println(b)
}
func parol() {
	var a string
	fmt.Scan(&a)
	b := []rune(a)
	lat := true
	for _, val := range b {
		if !unicode.Is(unicode.Latin, val) && !unicode.Is(unicode.Number, val) {
			lat = false
		}
	}
	if lat && len(b) >= 5 {
		fmt.Print("Ok")
	} else {
		fmt.Println("Wrong password")
	}
}

func main() {
	var a, b string
	fmt.Scan(&a)
	for _, v := range a {
		b = string(v) + b
	}
	if b == a {
		fmt.Println("Палиндром")
	} else {
		fmt.Println("Нет")
	}
}

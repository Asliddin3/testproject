package main

import "fmt"

func main() {
	fmt.Println("Hello leetcode problems ")
	var a int
	fmt.Scan(&a)
	arr := make([]int, a)

	for i := 0; i < a; i++ {
		fmt.Scan(&arr[i])
	}
	fmt.Println(arr[3])

	array := [5]int{}
	var b int
	for i := 0; i < 5; i++ {
		fmt.Scan(&b)
		array[i] = b
	}
	var c int
	fmt.Scan(&a)
	g := make([]int, a)
	for i := 0; i < a; i++ {
		fmt.Scan(&c)
		g[i] = c
	}
	for i := 0; i < a; i += 2 {
		fmt.Print(g[i])
		fmt.Print(" ")
	}

	fmt.Scan(&a)
	f := make([]int, a)
	for i := 0; i < a; i++ {
		fmt.Scan(&c)
		f[i] = c
	}
	sum := 0
	for i := 0; i < a; i++ {
		if f[i] > 0 {
			sum++
		}
	}
	fmt.Println(sum)

}

package main

import (
	"bufio"
	"fmt"
	. "fmt"
	"os"
	"strconv"
	"unicode"
)

func sqrtNum() {
	var a int
	Scan(&a)
	for a > 9 {
		num := a % 10
		a = a / 10
		a = a + num
	}
	Print(a)
}
func korov() {
	var a int
	Scan(&a)
	var crow string
	d := a % 10
	switch {
	case (d >= 5 && d <= 9) || d == 0 || a/10 == 1:
		crow = "korov"
	case d == 1:
		crow = "korova"
	default:
		crow = "korovy"
	}
	Printf("%d %s", a, crow)
}
func increament() {
	var a, b int
	Scan(&a)
	for b = 1; a >= b; b = b * 2 {
		Printf("%d ", b)
	}
}
func fibonchi() {
	f1 := 0
	f2 := 1
	var a, count int
	Scan(&a)
	count = 0
	for a >= f2 {
		f1, f2 = f2, f1+f2
		count++
	}
	if a == f1 {
		Print(count)
	} else {
		Print(-1)
	}
}
func nums() {
	var a int
	Scan(&a)
	Printf("%b", a)
}

func strtoint() {
	var a, b, c string
	fmt.Scan(&a)
	fmt.Scan(&c)
	b = ""
	for _, val := range a {
		if string(val) != c {
			b = b + string(val)
		}
	}

	num, _ := strconv.Atoi(b)
	fmt.Println(num)
}
func minimumFromFour() int {
	var a, min int
	fmt.Scan(&min)
	for i := 0; i < 3; i++ {
		fmt.Scan(&a)
		if a < min {
			min = a
		}
	}
	return min
}
func vote(x int, y int, z int) int {
	if x+y+z >= 2 {
		return 1
	} else {
		return 0
	}
}
func fibonacci(n int) int {
	f1, f2 := 0, 1
	for n > 0 {
		f1, f2 = f2, f1+f2
		n--
	}
	return f1
}
func sumInt(a ...int) (int, int) {
	suma := 0
	for _, v := range a {
		suma += v
	}
	return len(a), suma
}
func test(x1 *int, x2 *int) {
	c := *x1 * *x2
	fmt.Println(c)
}
func test1(x1 *int, x2 *int) {
	*x1, *x2 = *x2, *x1
	fmt.Println(*x1, *x2)
}

type Shooter struct {
	On    bool
	Ammo  int
	Power int
}

func (b *Shooter) RideBike() bool {
	if b.Power > 0 && b.On == true {
		b.Power--
		return true
	} else {
		return false
	}
}

func (p *Shooter) Shoot() bool {
	if p.Ammo > 0 && p.On == true {
		p.Ammo--
		return true
	} else {
		return false

	}
}
func formating() {
	text, _ := bufio.NewReader(os.Stdin).ReadString('\n')
	runes := []rune(text)
	if unicode.IsUpper(runes[0]) && runes[len(runes)-1] == '.' {
		fmt.Println("Right")
	} else {
		fmt.Println("Wrong")
	}
}

func main() {
	var a, b, c int
	Scanf("%1d%1d%1d", &a, &b, &c)
	Print(a + b + c)

	Scanf("%1d%1d%1d", &a, &b, &c)
	Printf("%d%d%d", c, b, a)

	var h, m int
	Scan(&a)
	h = a / 3600
	m = (a % 3600) / 60
	Printf("It is %d hours %d minutes.", h, m)

	Scanf("%d %d %d", &a, &b, &c)
	if c*c == a*a+b*b {
		Print("Прямоугольный")
	} else {
		Print("Непрямоугольный")
	}

	Scanf("%d %d %d", &a, &b, &c)
	if a+b > c && a+c > b && b+c > a {
		Print("Существует")
	} else {
		Print("Не существует")
	}

	var t int
	Scanf("%d %d", &a, &b)
	t = a + b
	if t%2 == 0 {
		c := t / 2
		Print(c)
	} else {
		c := float64(t) / 2.0
		Printf("%.1f", c)
	}

}

func countZero() {
	var a int
	Scan(&a)
	b := make([]int, a)
	for i := 0; i < a; i++ {
		Scan(&b[i])
	}
	count := 0
	for _, val := range b {
		if val == 0 {
			count++
		}
	}
	Print(count)
}

func countMinimum() {
	var a int
	Scan(&a)
	b := make([]int, a)
	for i := 0; i < a; i++ {
		Scan(&b[i])
	}
	min := b[0]
	count := 0
	for _, val := range b {
		if min > val {
			count = 1
			min = val
		} else if min == val {
			count++
		}
	}
	Print(count)
}
